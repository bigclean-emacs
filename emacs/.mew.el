﻿;; mew-pop-size设置成0时，pop邮件大小没有限制
(setq mew-pop-size 0)
;; 不删除服务器上的邮件
(setq mew-pop-delete nil)

;; Password
;; WARNING: Password is stored in Emacs with RAW format.
(setq mew-use-cached-passed t)          ;; nil
(setq mew-passwd-timer-unit 999)
(setq mew-passwd-lifetime 999)

(setq mew-ssl-verify-level 0)
(if (string-equal system-type "windows-nt")
 (setq mew-prog-ssl  "C:/Program Files/stunnel/stunnel.exe")
 (setq mew-prog-html "usr/bin/stunnel"))

;; my gmail settings
(setq mew-config-alist
      '(("default"
         ("name"                 . "bigclean")
         ("user"                 . "zhengjujie")
         ("mail-domain"          . "gmail.com")
         ("pop-auth"             . pass)
         ("pop-ssl"              . t)
         ("pop-user"             . "zhengjujie@gmail.com")
         ("pop-server"           . "pop.gmail.com")
         ("pop-ssl-port"         . "995")
         ("smtp-user"            . "zhengjujie@gmail.com")
         ("smtp-ssl"             . t)
         ("smtp-server"          . "smtp.gmail.com")
         ("smtp-port"            . "465"))))

;; refile
(setq mew-refile-guess-alist
      '(("To:"
         ("help-octave@octave.org"            . "+math/octave")
         ("pongba@googlegroups.com"           . "+program/pongba")
         ("gtdlife@googlegroups.com"          . "+life/gtd")
         ("maxima@math.utexas.edu"            . "+math/maxima")
         ("mingw-users@lists.sourceforge.net" . "+program/mingw")
         ("cn.bbs.comp.emacs@googlegroups.com". "+program/emacs")
         ("help-gnu-emacs@gnu.org"            . "+program/emacs")
         ("mew-int@mew.org"                  . "+program/mew")
         ("vim-cn@googlegroups.com"           . "+program/vim"))
        ("Cc:"
         ("help-octave@octave.org"            . "+math/octave")
         ("pongba@googlegroups.com"           . "+program/pongba")
         ("gtdlife@googlegroups.com"          . "+life/gtd")
         ("maxima@math.utexas.edu"            . "+math/maxima")
         ("mingw-users@lists.sourceforge.net" . "+program/mingw")
         ("cn.bbs.comp.emacs@googlegroups.com". "+program/emacs")
         ("help-gnu-emacs@gnu.org"            . "+program/emacs")
         ("mew-int@mew.org"                   . "+program/mew")
         ("vim-cn@googlegroups.com"           . "+program/vim"))
        (nil . "+inbox")))

;; 设置refile rule,summary下M-o即可
(setq mew-use-fast-refile t)
(setq mew-use-node-folder nil)
(setq mew-refile-ctrl-multi t)
(defvar mew-refile-guess-control
  '(mew-refile-guess-by-alist
    mew-refile-ctrl-throw
    mew-refile-guess-by-newsgroups
    mew-refile-guess-by-folder
    mew-refile-ctrl-throw
    mew-refile-ctrl-auto-boundary
    mew-refile-guess-by-thread
    mew-refile-ctrl-throw
    mew-refile-guess-by-from-folder
    mew-refile-ctrl-throw
    mew-refile-guess-by-from
    mew-refile-ctrl-throw
    mew-refile-guess-by-default))

;; some setting for summary mode
(setq mew-use-unread-mark t)

;; mew-w3m,support html 
;;用w3m来读html格式邮件
(require 'mew-w3m)
(setq mew-prog-html '(mew-mime-text/html-w3m nil nil)) 

;(setq mew-mime-multipart-alternative-list '("Text/Html" "Text/Plain" "*."))
;(condition-case nil
 ;   (require 'mew-w3m)
  ;(file-error nil))
