;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; appearance.el -- font and themes settings
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 03:57:30 星期六 by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; -----------------------------------------------------------------------------
;;
;; Font settings for English and Chinese individual
;; use different fonts in windows and unix
;; Note:in windows terminal,emacs can't display chinese character correctly
;; and this file is recommended used in Vista or Win7 because some fonts.
;;
;; -----------------------------------------------------------------------------
;; some fonts name and font size definition
(defconst windows-chinese-font "微软雅黑")
(defconst windows-latin-font "Consolas")
(defconst windows-japanese-font "Meiryo")
(defconst windows-korean-font "Malgun")
(defconst unix-chinese-font "WenQuanYi Zen Hei Mono")
(defconst unix-latin-font "Monospace")

(defconst latin-size 12)
(defconst cjk-size 12)

(if (window-system)
    (if (string-equal window-system "w32")
        (progn
	  ;; deafult,my emacs is running in Win7
          (set-default-font "Consolas-12")
	  ;; japanese font
          (set-fontset-font (frame-parameter nil 'font)
                            'kana ' ("Meiryo" . "unicode-bmp"))
	  ;; korean font
          ;; (set-fontset-font (frame-parameter nil 'font)
			    ;; 'hangul ' ("Malgun" . "unicode-bmp"))
          (set-fontset-font (frame-parameter nil 'font)
                            'han ' ("微软雅黑" . "unicode-bmp")))
      (progn
	;; emacs is running in x-window
        (set-default-font "Monospace-12")
        (set-fontset-font (frame-parameter nil 'font)
                          'han ' ("WenQuanYi Zen Hei Mono" . "unicode-bmp"))))
  ;; emacs is running in terminal or cmd in wondows
  (if (string-equal system-type "windows-nt")
      (set-default-font "Consolas-12")
	(set-default-font "Monospace-12")))

;; -----------------------------------------------------------------------------
;; color theme 
;; -----------------------------------------------------------------------------
(defconst theme "zenburn")
	
(add-to-list 'load-path
			 "~/.emacs.d/lisps/color-theme-6.6.0")
(add-to-list 'load-path
			 "~/.emacs.d/lisps/color-theme-6.6.0/themes")
(require 'color-theme)
(color-theme-initialize)
(require 'zenburn)
;;;; Some elegant and fitable color theme
;; (color-theme-dark-blue)
;; (color-theme-gray30) 
;; (color-theme-deep-blue)
;; (color-theme-subtle-blue)
;; (color-theme-gnome2)

;;;; some collected themes
;;;; only use theme when x-windows
(if (window-system)
    (color-theme-arjen-2))
;; (color-theme-zenburn)
;; (color-theme-tango)
;; (color-theme-tango-2)
;; (color-theme-tango-light)
;; (color-theme-dark-vee)
;; (color-theme-hober2)
;;;; very lovely theme
;; (pink-bliss)
