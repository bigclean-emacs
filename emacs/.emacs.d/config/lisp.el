 ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; lisp.el -- lisp(sbcl) and scheme settings
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 03:53:56 星期六 by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 

;; -----------------------------------------------------------------------------
;; lisp settings for slime
;; Note: lisp completetion settings are in c.el.
;; -----------------------------------------------------------------------------
;; (add-to-list 'load-path
;;              "~/.emacs.d/lisps/slime")
;; ;; (setq inferior-lisp-program "C:/sbcl/sbcl.exe -core C:/sbcl/sbcl.core") 
;; (setq inferior-lisp-program "C:/sbcl/sbcl.exe")
;; (require 'slime)
;; (slime-setup)
;; (setq slime-startup-animation nil)

;; -----------------------------------------------------------------------------
;; auto-indention for elisp
;; If you want automatic indentation, try to use C-j instead of RET for a while. 
;; C-j usually runs newline-and-indent.  
;; -----------------------------------------------------------------------------
;; Replace lisp-mode-hook with the appropriate hook
(add-hook 'lisp-mode-hook '(lambda ()
                             (local-set-key (kbd "RET") 'newline-and-indent)))

;; -----------------------------------------------------------------------------
;;scheme seetings for mzscheme
;; -----------------------------------------------------------------------------
;; (setq scheme-program-name "mzscheme")
(require 'quack)

;; -----------------------------------------------------------------------------
;; haskelle mode
;; -----------------------------------------------------------------------------
;; (add-to-list 'load-path
;;              "~/.emacs.d/lisps/haskell-mode-2.4")
;; (load "~/.emacs.d/lisp/haskell-mode-2.4/haskell-site-file")
;; (autoload 'haskell-refac-mode "haskell-refac"
;;   "Minor mode for refactoring Haskell programs" t)
;; (add-hook 'haskell-mode-hook 'turn-on-font-lock)        ; 高亮模式
;; (add-hook 'haskell-mode-hook 'turn-on-haskell-indent)   ; 智能缩进模式
;; (add-hook 'haskell-mode-hook 'turn-on-haskell-ghci)     ; GHCi 交互模式
;; (add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode) ; 文档模式
;; (add-hook 'haskell-mode-hook 'haskell-refac-mode)       ; 重构
;; (add-hook 'haskell-mode-hook 'hs-lint-mode-hook)        ; 代码建议
