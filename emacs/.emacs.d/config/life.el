;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; life.el -- mainly for muse and org for daily use
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 03:53:38 星期六 by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; -----------------------------------------------------------------------------
;;;;emacs muse setting,for writing
;; -----------------------------------------------------------------------------
(add-to-list 'load-path 
			       "~/.emacs.d/lisps/muse-3.12/lisp")
(require 'muse-mode)                              ; load authoring mode 
(require 'muse-html)                              ; load publishing styles I use
(require 'muse-texinfo)                           ; load Info publishing style
(require 'muse-docbook)                           ; load DocBook publishing style
(require 'muse-latex)                             ; load LaTeX/PDF publishing styles
(require 'muse-latex2png)                         ; publish <latex> tags
(require 'muse-project)                           ; publish files in projects
(require 'muse-wiki)                              ; load Wiki support
(require 'muse-xml)                               ; load XML support

(setq muse-project-alist
      '(("blogs"
         ("~/schemeblog" :default "index")
         (:base "html" :path "~/schemeblog/html"))))
         ;; (:base "pdfcjk" :path "~/notes/pdf")
         ;; (:base "info" :path "~/schemeblog/info")
         ;; (:base "texi" :path "~/schemeblog/texi"))))

;; -----------------------------------------------------------------------------
;; org-mode settings
;; -----------------------------------------------------------------------------
;;只高亮显示最后一个代表层级的
(setq org-hide-leading-stars t) 
(define-key global-map "\C-ca" 'org-agenda) ;;C-c a进入日程表
;; 给已完成事项打上时间戳。可选note，附加注释
(setq org-log-done 'time)
