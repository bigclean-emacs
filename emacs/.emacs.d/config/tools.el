﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; tools.el -- small utility for emacs
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 14:30:30 Saturday by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; -----------------------------------------------------------------------------
;; these elisp tpols are part of Emacs
;; -----------------------------------------------------------------------------
;; Display line number
(require 'linum)
;; disable number line display mode
;; (global-linum-mode t)

;; Note:linum-mode conficts with company mode
;; enable linum mode when program
(dolist (hook (list
               'emacs-lisp-mode-hook
               'lisp-mode-hook
               'lisp-interaction-mode-hook
               'scheme-mode-hook
               'c-mode-common-hook
               'python-mode-hook))
  (add-hook hook 'linum-mode))

;; ido setting
(require 'ido)
(ido-mode t)
;; ido 会生成一个缓存文件 ido-save-directory-list-file，
;; 默认位置是 ~/.ido.last，如果不喜欢主目录下隐藏文件太多，可以修改它的位置：
(setq ido-save-directory-list-file "~/.emacs.d/.ido.last")
(ido-everywhere 1)

;; Settings for ibuffer
;;把 C-x C-b 那个普通的 buffer menu 换成非常方便的 ibuffer 啦！ 
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Flyspell for aspell
(require 'flyspell)
;; Declare the flyspell-mode function as auto-load
;; use Flyspell with M-x flyspell-mode.
(autoload 'flyspell-mode "flyspell" "On-the-fly spelling checker." t)
(autoload 'flyspell-delay-command "flyspell" "Delay on command." t)
(autoload 'tex-mode-flyspell-verify "flyspell" "" t) 
;; 用aspell替换ispell, 更加智能
(setq-default ispell-program-name "aspell")     
;; 修复aspell与ispell冲突的bug
(setq-default ispell-extra-args '("--reverse")) 
;; 设置英文词典,you must specify or you'll get error
(setq ispell-dictionary "english")      


;; Woman 是一个纯 Elisp 写的 man page 解释器，很强大，很好用。
;; Let woman not open new frame
(require 'woman)
(setq woman-use-own-frame nil)
(setq woman-fill-column 75)
;; mingw and gnuwin32 as unix tools in Windows
(if (string-equal system-type "windows-nt") ; evaluate if it's working in Windows
    (setq woman-manpath '("C:/MinGW/man"
                          "C:/GnuWin32/man")))

;; -----------------------------------------------------------------------------
;; these elisp tools are not part of Emacs
;; you must install it individual
;; -----------------------------------------------------------------------------
;; Hihglight output                        
(require 'htmlize) ;Key bindings is M-x htmlize-buffer

;; Emacs tabber support 
(require 'tabbar)
(tabbar-mode)
;; (global-set-key (kbd "C-=") 'tabbar-backward-group)
;; (global-set-key (kbd "C--") 'tabbar-forward-group)
;; (global-set-key (kbd "C-0") 'tabbar-backward)
;; (global-set-key (kbd "C-9") 'tabbar-forward) 

;; multi-term,extending term
(require 'multi-term)
(setq multi-term-program "/bin/bash")
(custom-set-variables
     '(term-default-bg-color "#000000")        ;; background color (black)
     '(term-default-fg-color "#dddd00"))       ;; foreground color (yellow)

;; header2 elisp,it's now convinent to insert file header
(require 'header2)
(add-hook 'write-file-hooks 'auto-update-file-header)

;; highlight-parentheses
(require 'highlight-parentheses)
;; (highlight-parentheses-mode t)
;; needs to learn why should be hooked
(add-hook 'find-file-hook 'highlight-parentheses-mode t)

;; highlight-symbol
(require 'highlight-symbol)
(global-set-key [(control f3)] 'highlight-symbol-at-point)
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'highlight-symbol-prev)

;; -----------------------------------------------------------------------------
;; mew initial settings 
;; -----------------------------------------------------------------------------
(autoload 'mew "mew" nil t)
(autoload 'mew-send "mew" nil t)

;; Optional setup (Read Mail menu for Emacs 21):
(if (boundp 'read-mail-command)
    (setq read-mail-command 'mew))

;; Optional setup (e.g. C-xm for sending a message):
(autoload 'mew-user-agent-compose "mew" nil t)
(if (boundp 'mail-user-agent)
    (setq mail-user-agent 'mew-user-agent))
(if (fboundp 'define-mail-user-agent)
    (define-mail-user-agent
      'mew-user-agent
      'mew-user-agent-compose
      'mew-draft-send-message
      'mew-draft-kill
      'mew-send-hook))

;; -----------------------------------------------------------------------------
;; Dictionary for looking up 
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
			 "~/.emacs.d/lisps/dictionary-1.8.7")
(autoload 'dictionary-search "dictionary"
  "Ask for a word and search it in all dictionaries" t)
(autoload 'dictionary-match-words "dictionary"
  "Ask for a word and search all matching words in the dictionaries" t)
(autoload 'dictionary-lookup-definition "dictionary"
  "Unconditionally lookup the word at point." t)
(autoload 'dictionary "dictionary"
  "Create a new dictionary buffer" t)
(autoload 'dictionary-mouse-popup-matching-words "dictionary"
  "Display entries matching the word at the cursor" t)
(autoload 'dictionary-popup-matching-words "dictionary"
  "Display entries matching the word at the point" t)
(autoload 'dictionary-tooltip-mode "dictionary"
  "Display tooltips for the current word" t)

;; (setq dictionary-server "localhost") ;使用本地 server
(global-set-key (kbd "C-c s") 'dictionary-search)
(global-set-key (kbd "C-c m") 'dictionary-match-words)
;; 打开 tooltip 的功能
(setq dictionary-tooltip-dictionary "wn")
(require 'dictionary)
(global-dictionary-tooltip-mode t)
