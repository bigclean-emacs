;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; subject.el -- octave,maxima and haskell interior settings
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 03:54:17 星期六 by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;; -----------------------------------------------------------------------------
;; octave mode
;; -----------------------------------------------------------------------------
;; using Octave mode for all `.m' files
(autoload 'octave-mode "octave-mod" nil t)
(setq auto-mode-alist 
      (cons '("\\.m$" . octave-mode) auto-mode-alist))
;; turn on the abbrevs, auto-fill and font-lock features automatically
(add-hook 'octave-mode-hook
          (lambda ()
            (abbrev-mode 1)
            (auto-fill-mode 1)
            (if (eq window-system 'x)
                (font-lock-mode 1))))

;; -----------------------------------------------------------------------------
;; maxima mode,without latex support
;; -----------------------------------------------------------------------------
;; (add-to-list 'load-path
;;              "C:/Program Files/Maxima-5.19.2/share/maxima/5.19.2/emacs")
(require 'maxima)
(autoload 'maxima-mode "maxima" "Maxima mode" t)
(autoload 'maxima "maxima" "Maxima interaction" t)

;; -----------------------------------------------------------------------------
;; gnuplot mode
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
				"~/.emacs.d/lisps/gnuplot-mode.0.6.0")
(autoload 'gnuplot-mode "gnuplot" "gnuplot major mode" t)
(autoload 'gnuplot-make-buffer "gnuplot" "open a buffer in gnuplot mode" t)
(setq auto-mode-alist (append '(("\\.gp$" . gnuplot-mode)) auto-mode-alist)) 

;; -----------------------------------------------------------------------------
;; auctex for latex
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
             "~/.emacs.d/lisps/auctex-11.85-e23.1-msw/site-lisp/site-start.d")
(load "auctex.el" nil t t)
(load "preview-latex.el" nil t t)
;; load miktex when used in windows
(if (string-equal system-type "windows-nt")
    (require 'tex-mik))

;; basic settings
(setq TeX-auto-save t)
(setq TeX-parse-self t)
;; (setq-default TeX-master nil)

(add-hook 'LaTeX-mode-hook 'auto-fill-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)

(add-hook 'LaTeX-mode-hook
          (lambda()
            (define-key LaTeX-mode-map (kbd "TAB") 'TeX-complete-symbol)
            (TeX-PDF-mode t)
            (setq TeX-save-query  nil )
            (setq TeX-show-compilation t)))

;; 增加 math, reftex 等模式，有数学符号的栏目辅助输入等等
(add-hook 'LaTeX-mode-hook
          (lambda ()
            (imenu-add-menubar-index)
            (turn-on-reftex)
            (turn-on-auto-fill)))
