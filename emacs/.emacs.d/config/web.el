;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; web.el -- w3m,erc and jabber for web applications
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 03:56:58 星期六 by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; -----------------------------------------------------------------------------
;; erc settings
;; -----------------------------------------------------------------------------
(require 'erc)
;; erc-ring 提供了输入历史回滚的功能
(require 'erc-ring)                                                    
;; 提供了对聊天内容进行 fill 的功能
(require 'erc-fill)                                                    
(require 'erc-autoaway)
(require 'erc-log)                                                     

;; console emacs can't load smiley elisp
(if (window-system)
    (require 'smiley))

;; spell check
(erc-spelling-mode 1)                                                  

(setq erc-autojoin-mode t)                                             
(setq erc-autoaway-mode t)                                            

;; 在隐藏的Buffer中加入
(setq erc-join-buffer 'bury)                                           
(setq erc-server-auto-reconnect t)                                     

;; 设置 nickname 右对齐
(setq erc-fill-function 'erc-fill-static)                             
;; 最右端在 15 个字符的地方。
(setq erc-fill-static-center 15)                                       
;; (setq erc-fill-static-center 0)              ; 中间填充
;; (setq erc-fill-column 100)                   ; 折叠列数

;; 插入时间戳的列数
(setq erc-timestamp-right-column 110)                                  
;; 时间戳显示格式
(setq erc-timestamp-format "[%H:%M:%S]")                               
;; 右边时间戳显示格式
(setq erc-timestamp-format-right " [%H:%M:%S]")                        
;; 插入时间戳的方式(右边)
(setq erc-insert-timestamp-function 'erc-insert-timestamp-right)       
;; 插入离开时间戳的方式(右边)
(setq erc-insert-away-timestamp-function 'erc-insert-timestamp-right)  

;; 在Mode-line显示频道信息
(setq erc-track-position-in-mode-line t)                               
(setq erc-server-coding-system 'utf-8)                                 

(setq erc-server "irc.freenode.net" 
      erc-port 6667 
      erc-nick "bigclean"
      erc-user-full-name "zhengjujie"
      erc-email-userid "zhengjujie@163.com"                            ; for when ident is not activated 
      erc-prompt-for-password t)                                       ; OPN doesn't require passwords

(setq erc-track-exclude-server-buffer t)                               ; Don't track server buffer
;; Don't track join/quit
(setq erc-track-exclude-types '("NICK" "333" "353" "JOIN" "PART" "QUIT"))

;; logging settings
(erc-log-enable)
(setq erc-enable-logging t                                             ; enable logs
      erc-log-mode nil                                                 ; 关闭日志模式
      erc-log-channels-directory  "~/.emacs.d/erc/logs"                ; 日志的记录目录
      erc-save-buffer-on-part t                                        ; logs automatically written when you part or quit a channel
      erc-log-file-coding-system 'utf-8)                               ; erc logs encodings

;; -----------------------------------------------------------------------------
;; jabber for Gtalk
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
             "~/.emacs.d/lisps/emacs-jabber-0.8.0")
(require 'jabber)
(setq jabber-account-list
      '(("zhengjujie@gmail.com" 
         (:network-server . "talk.google.com")
         (:connection-type . ssl))))

;; -----------------------------------------------------------------------------
;; emacs-w3m 
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
                    "~/.emacs.d/lisps/emacs-w3m")  	
(require 'w3m-load)

;; 存储设置
(setq w3m-default-save-directory "~/.emacs.d/w3m/download")              
(setq w3m-bookmark-file "~/.emacs.d/w3m/bookmark.html")                  
(setq w3m-cookie-file "~/.emacs.d/w3m/w3m-cookie")                       
;; 设定任务保存的文件位置
(setq w3m-session-file "~/.emacs.d//w3m/w3m-session")                    

;; env settings for w3m
(setq w3m-coding-system 'utf-8
      w3m-language 'utf-8
      w3m-file-coding-system 'utf-8
      w3m-file-name-coding-system 'utf-8
      w3m-input-coding-system 'utf-8
      w3m-output-coding-system 'utf-8
      w3m-terminal-coding-system 'utf-8)

(setq w3m-use-favicon t)
(setq w3m-command-arguments '("-cookie" "-F"))

(setq w3m-use-cookies t)                                                 
(setq w3m-use-header-line-title t)                                       
(setq w3m-cookie-accept-bad-cookies t)                                   
(setq w3m-view-this-url-new-session-in-background t)                     
(setq w3m-new-session-in-background t)                                   

;; 上次浏览记录的时间显示格式
(setq w3m-session-time-format "%Y-%m-%d %A %H:%M")                       

;; display images
;; (setq w3m-default-display-inline-images t)                              

;; 使用网站图标的缓存文件
(setq w3m-favicon-use-cache-file t)                                      
;; 不在mode-line显示网站图标
(setq w3m-show-graphic-icons-in-mode-line nil)                          

;; 浏览历史记录的最大值
(setq w3m-keep-arrived-urls 50000)                                       
;; 缓存的大小
(setq w3m-keep-cache-size 1000)                                          

;; set browser homepage 
(setq w3m-home-page "http://forum.ubuntu.org.cn")
;; (setq w3m-home-page "http://localhost:8080/")
;; (setq w3m-home-page "http://linuxtoy.org")

;; -----------------------------------------------------------------------------
;; emacs-wget 
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
				"~/.emacs.d/lisps/emacs-wget-0.5.0")
(autoload 'wget "wget" "wget interface for Emacs." t) 
(autoload 'wget-web-page "wget" "wget interface to download whole web page." t) 
(load "w3m-wget") 
(setq wget-download-directort "~/.emacs.d/w3m/download")
