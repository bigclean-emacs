﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; c.el -- program setting for c
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 03:51:32 星期六 by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; -----------------------------------------------------------------------------
;; c and c++ style
;; -----------------------------------------------------------------------------
(require 'cc-mode)
;; enable fold mode
(add-hook 'c-mode-hook 'hs-minor-mode)
(add-hook 'c++-mode-hook 'hs-minor-mode)  

;; c style settings
(defun my-c-mode-common-hook()
  (c-set-style "k&r")
  ;; (c-set-style "ellemetel")
  (setq default-tab-width 4)
  (setq tab-width 4)
  (hs-minor-mode 4)
  (setq c-tab-always-indent t)
  ;;; hungry-delete and auto-newline
  (c-toggle-auto-state)
  (c-toggle-hungry-state)
  (which-function-mode)
  ;;按键定义
  (define-key c-mode-base-map [(control \`)] 'hs-toggle-hiding)
  (define-key c-mode-base-map [(return)] 'newline-and-indent)
  ;;预处理设置
  (setq c-macro-shrink-window-flag t)
  (setq c-macro-preprocessor "cpp")
  (setq c-macro-cppflags " ")
  (setq c-macro-prompt-flag t)
  (setq hs-minor-mode t)
  (setq abbrev-mode t))
(add-hook 'c-mode-common-hook 'my-c-mode-common-hook)

;; c++ style
(defun my-c++-mode-hook()
  (setq tab-width 4 indent-tabs-mode nil)
  (c-set-style "stroustrup")
  ;;  (define-key c++-mode-map [f3] 'replace-regexp)
  )
(add-hook 'c++-mode-hook 'my-c-mode-common-hook)

;; -----------------------------------------------------------------------------
;; settings for cedet and ecb
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
                    "~/.emacs.d/lisps/cedet-1.0pre6")   
(load-file "~/.emacs.d/lisps/cedet-1.0pre6/common/cedet.el")
(global-ede-mode 1) 
;; 设定 semantic 生成的 tag 文件保存目录
(setq semanticdb-default-save-directory "~/.emacs.d/semanticdb") 
;; 设置semantic分析代码文法的方式
(semantic-load-enable-code-helpers) 
(global-srecode-minor-mode 1) 

;; -----------------------------------------------------------------------------
;; ecb settings
;; -----------------------------------------------------------------------------
;; (add-to-list 'load-path
;;              "~/.emacs.d/lisps/ecb-2.40")
;; (load-file "~/.emacs.d/lisps/ecb-2.40/ecb.el")

;; javadoc and doxygeng
;; doc-mode must load after cedet
(require 'doc-mode)
(add-hook 'c-mode-common-hook 'doc-mode)

;; -----------------------------------------------------------------------------
;; compeletion
;; -----------------------------------------------------------------------------
;; first choice is auto-compression
;; but in cc-mode,i use company-mode instead
(require 'auto-complete)
;; disable auto-complete mode default
;; (global-auto-complete-mode t)           
;; Use C-n/C-p to select candidates
(define-key ac-complete-mode-map "\C-n" 'ac-next)
(define-key ac-complete-mode-map "\C-p" 'ac-previous)

;; -----------------------------------------------------------------------------
;; setting for company-mode,auto-compeletion
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
             "~/.emacs.d/lisps/company-0.4.3")
(autoload 'company-mode "company" nil t)
;; enable company mode only in c,lisp,scheme and python
(dolist (hook (list
               'emacs-lisp-mode-hook
               'lisp-mode-hook
               'lisp-interaction-mode-hook
               'scheme-mode-hook
               'c-mode-common-hook
               'python-mode-hook))
  (add-hook hook 'company-mode))
;; 这行代码是告诉 company-mode 扩展包，在弹出自动补全窗口之时莫要犹豫
(setq company-idle-dalay t)              
