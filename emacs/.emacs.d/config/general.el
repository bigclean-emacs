﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; general.el -- some general settings for emacs
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 02:31:09 星期六 by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; -----------------------------------------------------------------------------
;; coding system
;; thanks William Xu <william.xwl@gmail.com>
;; -----------------------------------------------------------------------------

;(case system-type
;  ((windows-nt)
;   (prefer-coding-system 'gbk))
;  (t
;   (prefer-coding-system 'utf-8)))

(if (string-equal system-type "windows-nt")
    (prefer-coding-system 'gbk)
  (prefer-coding-system 'utf-8))

;; (require 'auto-enca)
(setq file-coding-system-alist
      '(("\\.dz\\'" no-conversion . no-conversion)
	("\\.g?z\\(~\\|\\.~[0-9]+~\\)?\\'" no-conversion . no-conversion)
	("\\.tgz\\'" no-conversion . no-conversion)
	("\\.tbz\\'" no-conversion . no-conversion)
	("\\.bz2\\'" no-conversion . no-conversion)
	("\\.Z\\(~\\|\\.~[0-9]+~\\)?\\'" no-conversion . no-conversion)
	("\\.elc\\'" emacs-mule . emacs-mule)
	("\\.utf\\(-8\\)?\\'" . utf-8)
	("\\(\\`\\|/\\)loaddefs.el\\'" raw-text . raw-text-unix)
	("\\.tar\\'" no-conversion . no-conversion)
	("\\.po[tx]?\\'\\|\\.po\\." . po-find-file-coding-system)
	("\\.\\(tex\\|ltx\\|dtx\\|drv\\)\\'" . latexenc-find-file-coding-system)

        ;; ("" undecided)
	("" utf-8 . utf-8)
        ;; ("" . enca-detect-coding)
        ))

;; -----------------------------------------------------------------------------
;; Appearance Settings 外观
;; -----------------------------------------------------------------------------
;; disable toolbar,menubar and scrollbar
(tool-bar-mode -1)
;; (menu-bar-mode nil)
(scroll-bar-mode nil)
;; 禁用启动画面
(setq inhibit-startup-message t)                    
;; 禁止在草稿缓存里面显示处始化信息
;; (setq initial-scratch-message nil)      
     
;; Display time 显示时间
;; Time format 时间格式
(display-time-mode 1)
(setq display-time-24hr-format t)
(setq display-time-day-and-date t)
;; Display time information in modeline.
(setq display-time-format "%Y-%m-%d %A %I:%M")
(display-time)
;; Title coulum display 标题栏显示
(setq frame-title-format "bigclean@%b")              
;; 自动备份到一个目录下去
(setq backup-directory-alist (quote (("." . "~/.emacs.d/backups"))))

;; -----------------------------------------------------------------------------
;; Basic settings
;; -----------------------------------------------------------------------------
;; personal information
(setq user-full-name "bigclean")
(setq user-mail-address "zhengjujie@gmail.com")

;; 打开图片显示功能
(auto-image-file-mode t)                            

;; 以 y/n代表 yes/no
(fset 'yes-or-no-p 'y-or-n-p)                       

;; 语法加亮
(global-font-lock-mode t)              
(put 'set-goal-column 'disabled nil)

;; 开启变窄区域
(put 'narrow-to-region 'disabled nil)               
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'LaTeX-hide-environment 'disabled nil)

(blink-cursor-mode -1)                               ;; 指针不闪动
(auto-fill-mode 1)                                   ;; fill 相关。
(setq default-justification 'full)

;; 自动启用临时标记模式(高亮显示区域选择)
(transient-mark-mode t)                              

;; Display column number in modeline.
(column-number-mode t)

;; 高亮显示成对括号，但不来回弹跳
(show-paren-mode t)                                  
(setq show-paren-style 'parentheses)

;; 在状态条上显示当前光标在哪个函数体内部
(which-function-mode t)                              

;; 打开压缩文件时自动解压缩
(auto-compression-mode 1)                            

;; 设定自动缩进的注释风格
(setq-default comment-style 'indent)                 

;; 关闭烦人的出错时的提示声
(setq ring-bell-function 'ignore)                    

;; 设置默认地主模式为TEXT模式
(setq default-major-mode 'text-mode)                

;; 粘贴于光标处,而不是鼠标指针处
(setq mouse-yank-at-point t)                        

;; 光标在 TAB 字符上会显示为一个大方块
(setq x-stretch-cursor t)                           

;; 支持emacs和外部程序的粘贴
(setq x-select-enable-clipboard t)                 

;; 光标靠近鼠标指针时，让鼠标指针自动让开，别挡住视线
(mouse-avoidance-mode 'animate)                      
	
;; 禁止显示鼠标指针
(setq void-text-area-pointer nil)                   

;; 255 available undo-s. 
(setq kill-ring-max 255)                             

;; Begin scrolling when the cursor is 5 lines from the bottom.
(setq next-screen-context-lines 5)                   

;; 默认显示 80列就换行 
(setq default-fill-column 80)                        

;; 自动换行模式
(add-hook 'text-mode-hook 'turn-on-auto-fill)        

;; time-stamp
(add-hook 'write-file-hooks 'time-stamp)
(setq time-stamp-format "%02m/%02d/%04y %02H:%02M:%02S %:a by %u")

;; -----------------------------------------------------------------------------
;; Indent 缩进设置
;; -----------------------------------------------------------------------------
(setq-default indent-tabs-mode t)                   ;; 默认不用空格替代TAB
(setq default-tab-width 4)                          ;; 设置TAB默认的宽度
(setq tab-width 4)
(setq-default indent-tabs-mode nil)                 ;;  Make _Tab_ indent in some modes.
(dolist (hook (list                                 ;; 设置用空格替代TAB的模式
               'emacs-lisp-mode-hook
               'lisp-mode-hook
               'lisp-interaction-mode-hook
               'scheme-mode-hook
               'c-mode-hook
               'c++-mode-hook
               ))
(add-hook hook '(lambda () (setq indent-tabs-mode nil))))

;;; ### Advice ###
;;; --- 各种emacs行为建议
;; 在特定地模式下粘贴时自动缩进
(defadvice yank (after indent-region activate)
  "To make yank content indent automatically."
  (if (member major-mode '(emacs-lisp-mode
                           scheme-mode
                           lisp-mode
                           lisp-interaction-mode
                           c-mode
                           c++-mode
                           objc-mode
                           latex-mode
                           plain-tex-mode))
      (indent-region (region-beginning) (region-end) nil)))

;;;在各种命令行界面的程序中识别密码提示符，等这些程序要求输入密码的时候， 
;;;Emacs 就会让你在 mini-buffer 里安全的输入。
;; Fringe
(setq fringe-mode 'left-only)
(setq-default right-fringe-width 0)
(setq default-indicate-buffer-boundaries '((top . left) (t . left)))
;;;打开 fringe 显示。Fringe 是显示在 window 左边或右面的一个小竖条，
;;;上面 可以显示现在是否是 buffer 的开头或结尾，还有结尾是否正确的回车了。
