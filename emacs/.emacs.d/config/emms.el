﻿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; emmms.el -- emms for mplayer and mp3info 
;; Author: Bigclean <zhengjujie@gmail>
;; Time-stamp: <11/14/2009 03:52:29 星期六 by bigclean>
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; -----------------------------------------------------------------------------
;; thanks William Xu(xwl)
;; -----------------------------------------------------------------------------
(add-to-list 'load-path
                    "~/.emacs.d/lisps/emms-3.0")  	
(require 'emms-setup)
(emms-standard)					
;; 调整音量
;; no cli volume setup tools in windows
;(require 'emms-volume)
;; 给音乐打分
(require 'emms-score)
(emms-score 1)
;; 自动识别音乐标签的编码
(require 'emms-i18n)
;; 自动保存和导入上次的播放列表
(require 'emms-history)

;; My musics 添加音乐文件默认的目录
;; use faster finding facility if you have GNU find
;(setq emms-source-file-directory-tree-function 'emms-source-file-directory-tree-find)
(setq emms-source-file-default-directory "J:/album")

;; music players,default is mplayer
(setq emms-player-list
      '(emms-player-mplayer
	    emms-player-mpg321
        emms-player-ogg123))

;; coding settings
(setq emms-info-mp3info-coding-system 'gbk
      emms-cache-file-coding-system 'utf-8
      ;; emms-i18n-default-coding-system '(utf-8 . utf-8)
      )

;; show emms mode line 
(require 'emms-mode-line)
;; (require 'emms-mode-line-icon)
(emms-mode-line 1)
;; Display emms playing time on mode line 
(require 'emms-playing-time)
(emms-playing-time 1)

;; format current track,only display title in mode line
(defun bigclean-emms-mode-line-playlist-current ()
  "Return a description of the current track."
  (let* ((track (emms-playlist-current-selected-track))
         (type (emms-track-type track))
         (title (emms-track-get track 'info-title)))
	(format "[ %s ]"
            (cond ((and title)
                   title)))))

(setq emms-mode-line-mode-line-function
      'bigclean-emms-mode-line-playlist-current)

(setq emms-show-format "Now playing: %s")                             
(setq emms-playlist-buffer-name "*Music*" 
	  emms-playing-time-style 'bar
	  emms-info-asynchronously nil)                                   

(require 'emms-info)
(require 'emms-info-mp3info)                                      
(setq emms-playlist-default-major-mode 'emms-playlist-mode)           
(add-to-list 'emms-info-functions 'emms-info-mp3info)

;; my customizable playlist format
(defun simple-emms-track-description-function (track)
  "Return a description of the current track."
  (let ((artist (emms-track-get track 'info-artist))
		(title (emms-track-get track 'info-title))
		(album (emms-track-get track 'info-album))
		(ptime (emms-track-get track 'info-playing-time)))
	(if title 
	  (format 
		"%-35s %-40s %-35s %5s:%-5s"
		(if artist artist "")
		(if title title "")
		(if album album "")
		(/ ptime 60)
		(% ptime 60)))))


(eval-after-load 'emms
  '(progn
     (setq xwl-emms-playlist-last-track nil)
     (setq xwl-emms-playlist-last-indent "\\")

	 (defun bigclean-emms-track-description-function (track)
	  "Return a description of the current track."
	  (let* ((artist (emms-track-get track 'info-artist))
			 (year (emms-track-get track 'info-year))
			 (ptime (emms-track-get track 'info-playing-time) )
			 (min (/ ptime 60))
			 (sec (% ptime 60))
			 (album (emms-track-get track 'info-album))
			 (tracknumber (emms-track-get track 'info-tracknumber))
			 (title (emms-track-get track 'info-title))
			 
			 ;; last track
			 (ltrack xwl-emms-playlist-last-track)
			 (lartist  (and ltrack (emms-track-get ltrack 'info-artist)))
					
			 (lalbum (and ltrack (emms-track-get ltrack 'info-album)))

			 (same-album-p  (not (string= lalbum )
							 (string= album lalbum))))
	 
	 (format "%-20s%-60s%-35s%-15s%s"
	  (if artist artist "")
	  ;; Combine indention, tracknumber, title.
	  ;; (format "%s%s%-40s"
	  (concat
	   (if same-album-p ; indention by album
		(setq xwl-emms-playlist-last-indent
		 (concat " " xwl-emms-playlist-last-indent))
		(setq xwl-emms-playlist-last-indent "\\")
		"")
	   (if (and tracknumber ; tracknumber
			(not (zerop (string-to-number tracknumber))))
		(format "%02d." (string-to-number tracknumber))
		"")
	   title        ; title
	   )
	  ;; album
	  (concat "《" album "》")
	  (if year year "")
	  (if (or (> min 0)  (> sec 0))
	  (format "%02d:%02d" min sec)
	   ))))
	   
	   (setq emms-track-description-function
		'bigclean-emms-track-description-function)
	   ))                                       

;(setq emms-track-description-function
;      'simple-emms-track-description-function)                                       

; 设置EMMS缓存
(when (fboundp 'emms-cache)
  (emms-cache 1))
  
(add-hook 'emms-player-started-hook 'emms-show)

;; -----------------------------------------------------------------------------
;; 
;; global key-map
;; all global keys prefix is C-c e
;; compatible with emms-playlist mode keybindings
;; you can view emms-playlist-mode.el to get details about 
;; emms-playlist mode keys map
;; 
;; -----------------------------------------------------------------------------
(global-set-key (kbd "C-c e s") 'emms-stop)
(global-set-key (kbd "C-c e P") 'emms-pause)
(global-set-key (kbd "C-c e n") 'emms-next)
(global-set-key (kbd "C-c e p") 'emms-previous)
(global-set-key (kbd "C-c e f") 'emms-show)
(global-set-key (kbd "C-c e >") 'emms-seek-forward)
(global-set-key (kbd "C-c e <") 'emms-seek-backward)

;; these keys maps were derivations of above keybindings
(global-set-key (kbd "C-c e S") 'emms-start)
(global-set-key (kbd "C-c e g") 'emms-playlist-mode-go)
(global-set-key (kbd "C-c e t") 'emms-play-directory-tree)
(global-set-key (kbd "C-c e h") 'emms-shuffle)
(global-set-key (kbd "C-c e e") 'emms-play-file)
(global-set-key (kbd "C-c e l") 'emms-play-playlist)
(global-set-key (kbd "C-c e r") 'emms-toggle-repeat-track)
(global-set-key (kbd "C-c e R") 'emms-toggle-repeat-playlist)
(global-set-key (kbd "C-c e u") 'emms-score-up-playing)
(global-set-key (kbd "C-c e d") 'emms-score-down-playing)
(global-set-key (kbd "C-c e o") 'emms-score-show-playing)
