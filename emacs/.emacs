﻿;; author: Bigclean <zhengjujie@gmail.com>

;; Time-stamp: <11/14/2009 03:05:15 星期六 by bigclean>

;; -----------------------------------------------------------------------------
;; 
;; define some emacs related symbols
;; small utility are in ~/.emacs.d/site-lisp/
;; suite of lisps are in ~/.emacs.d/lisps/
;; config elisps are in ~/.emamcs.d/confifg
;;
;; -----------------------------------------------------------------------------

(defconst emacs-path "~/.emacs.d/")
(defconst emacs-small-utility "~/.emacs.d/site-lisp/")
(defconst my-emacs-lisps "~/.emacs.d/lisps/")
(defconst my-emacs-config "~/.emacs.d/config/")

;; Personal lisp utility 
;; (add-to-list 'load-path "~/.emacs.d/site-lisp/")
(add-to-list 'load-path emacs-small-utility)

;; ----------------------------------------------------------------------------- 
;; load emacs individual configuration files
;; -----------------------------------------------------------------------------
(load (concat my-emacs-config "appearance"))     ;; font and themes settings
(load (concat my-emacs-config "general"))        ;; General seeting for emacs
(load (concat my-emacs-config "tools"))          ;; some usefule lisps for emacs
(load (concat my-emacs-config "emms"))          ;; emms music settings
(load (concat my-emacs-config "web"))          ;; emacs-w3m,emacs-wget,erc,jabber(gtalk)
(load (concat my-emacs-config "life"))           ;; muse and org-mode settings
(load (concat my-emacs-config "c"))              ;; program for c
(load (concat my-emacs-config "subject"))        ;; maxima,octave,gnuplot and latex
;; (load (concat my-emacs-config "lisp"))           ;; lisp,scheme,haskell settings
